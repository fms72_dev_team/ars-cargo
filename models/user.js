var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var UserSchema = new mongoose.Schema({
    accessLevel: {
        type: Number,
        required: true,
        default: 0
    },
    banned: {
        type: Boolean,
        required: true,
        default: false
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    }
});

/*
0 - no access, just registered
1 - view map
2 - plan path
3 - manage robots and their tokens
4 - 
5 - root
*/

UserSchema.pre('save', function (next) {
    var user = this;
    var salt = bcrypt.genSaltSync(10);
    bcrypt.hash(user.password, salt, null, function (err, hash){
        if (err) {
            return next(err);
        }
        user.password = hash;
        next();
    })
});

UserSchema.statics.auth = function (username, password, callback) {
    User.findOne({ username: username }).exec(function (err, user) {
        if (err) {
            return callback(err)
        } else if (!user) {
            var err = new Error('User not found.');
            err.status = 401;
            return callback(err);
        }
        bcrypt.compare(password, user.password, function (err, result) {
            if (result === true) {
                return callback(null, user);
            } else {
                var err = new Error('Passwords does not match.');
                err.status = 403;
                return callback(err);
            }
        })
    });
}  

var User = mongoose.model('User', UserSchema);
module.exports = User;