var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var RobotSchema = new mongoose.Schema({
    class: {
        type: Number,
        required: true,
        default: 1
    },
    disabled: {
        type: Boolean,
        required: true,
        default: false
    },
    name: {
        type: String,
        unique: true,
        required: true,
        default: ""
    },
    token: {
        type: String,
        unique: true,
        required: true,
        trim: true
    }
});

/*
1 - robot with lift
0 - robot without lift
*/

var Robot = mongoose.model('Robot', RobotSchema);
module.exports = Robot;