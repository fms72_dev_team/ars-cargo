var config = require("./config.json");

var mongoose = require('mongoose');
var jwt = require('jsonwebtoken')
var app = require('express')();
var http = require('http').Server(app);
var WebSocketServer = require('websocket').server;

var MAP = require("./map.json");

var io = new WebSocketServer({
    httpServer: http
});

var path = require('path');

app.get('/', function(req, res){
    res.send("<h1>Get out of there! It\'s a private territory.</h1>");
});

var dbConfig = {
    URI: 'mongodb://127.0.0.1:27017/arscargo',
    OPTIONS: {
        useNewUrlParser: true
        // user: 'root',
        // pass: 'asarodip3',
        // auth: {
        //     authdb: 'admin'
        // }
    }
}  

mongoose.connect(dbConfig.URI, dbConfig.OPTIONS)
    .then(() =>  console.log('Database connection succeded'))
    .catch((err) => console.error(err));

var User = require('./models/user.js');
var Robot = require('./models/robot.js');

var RobotAuthHandler = require("./msgHandlers/robotAuthHandler");
var UserAuthHandler = require("./msgHandlers/userAuthHandler");
var UserLogoutHandler = require("./msgHandlers/userLogoutHandler");
var OdometryHandler = require("./msgHandlers/odometryHandler");
var UserRegisterHandler = require("./msgHandlers/userRegisterHandler");
var AddRobotHandler = require("./msgHandlers/addRobotHandler");
var RemoveRobotHandler = require("./msgHandlers/removeRobotHandler");
var SwitchDisableRobotHandler = require("./msgHandlers/switchDisableRobotHandler");

var clients = {};
var robots = {};

var clientCounter = 0;
io.on('request', function(request) {
    var socket = request.accept(null, request.origin);
    //var ID = (socket.id).toString().substr(0, 5);
    var ID = ++clientCounter;
    var time = (new Date).toLocaleTimeString();
    socket.sendUTF(JSON.stringify({event: 'connected'}));
    console.log("Connected ID: " + ID + ", time: " + time);	
    
    clients[ID] = {
        loggedin: false,
        type: "",
        socket: socket
    };
    socket.on('message', function (msg) {
        if(msg.type === 'utf8') {
            var time = (new Date).toLocaleTimeString();
            try {
                var msgDecoded = JSON.parse(msg.utf8Data);
                // console.log(msgDecoded);
                if(msgDecoded.event && msgDecoded.data) {
                    switch(msgDecoded.event) {
                        case "robotAuth":
                            RobotAuthHandler.handle(msgDecoded.data, ID, clients, robots);
                            break;
                        case "userAuth":
                            UserAuthHandler.handle(msgDecoded.data, ID, clients, MAP, robots);
                            break;
                        case "userRegister":
                            UserRegisterHandler.handle(msgDecoded.data, ID, clients);
                            break;
                        case "odometry":
                            OdometryHandler.handle(msgDecoded.data, ID, clients, robots);
                            break;
                        case "userLogout":
                            UserLogoutHandler.handle(msgDecoded.data, ID, clients);
                            break;
                        case "addRobot":
                            AddRobotHandler.handle(data, ID, clients);
                            break;
                        case "removeRobot":
                            RemoveRobotHandler.handle(data, ID, clients);
                            break;
                        default:
                            socket.sendUTF(JSON.stringify({event: "generalMessageError"}));
                            break;
                    }
                }
            }
            catch(e) {
                console.log('Exception ' + e.name + ":" + e.message + "\n" + e.stack);
            }
        }
    });
    socket.on('disconnect', function() {
		var time = (new Date).toLocaleTimeString();
        // io.sockets.json.send({'event': 'userSplit', data: {'name': ID, 'time': time}});
        if(clients[ID].loggedin)
            users[clients[ID].userId].online = false;
        delete clients[ID];
    });
});

http.listen(8080, function(){
    console.log('listening on *:8080');
});
  