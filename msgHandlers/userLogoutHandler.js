var jwt = require('jsonwebtoken');
var config = require("../config.json");
var User = require('../models/user.js');

function handle (data, ID, clients) {
    if(clients[ID].loggedin) {
        clients[ID].loggedin = false;
        clients[ID].socket.sendUTF(JSON.stringify({event: "userLogout", data: {result: true}}));
    }
    else
        clients[ID].socket.sendUTF(JSON.stringify({event: "userLogout", data: {result: false, message: "You must be logged in." }}));
}

module.exports.handle = handle;