var User = require('../models/user.js');

function handle (data, ID, clients) {
    if(!clients[ID].loggedin) {
        if(data.username && data.password) {
            var userData = {
                username: data.username,
                password: data.password
            }
            User.create(userData, function (err, user) {
                if (err) {
                    // console.log(userData);
                    console.log(err);
                    clients[ID].socket.sendUTF(JSON.stringify({event: 'userRegister', data: {result: false, message: 'This user already exists or your credentials are not valid.'}}));
                } else {
                    clients[ID].socket.sendUTF(JSON.stringify({event: 'userRegister', data: {result: true, message: 'Succesfully registered! Log in now.'}}));
                }
            });
        }
        else
            clients[ID].socket.sendUTF(JSON.stringify({event: "userRegister", data: {result: false, message: "Username or password is not provided."}}));
    }
    else
        clients[ID].socket.sendUTF(JSON.stringify({event: "userRegister", data: {result: false, message: "You are already registered and logged in." }}));
}

module.exports.handle = handle;