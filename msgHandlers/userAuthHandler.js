var jwt = require('jsonwebtoken');
var config = require("../config.json");
var User = require('../models/user.js');
var Robot = require('../models/robot.js');

function handle (data, ID, clients, MAP, robots) {
    if(!clients[ID].loggedin) {
        if(data.username && data.password) {
            User.auth(data.username, data.password, function (err, user) {
                if(!user)
                    return clients[ID].socket.sendUTF(JSON.stringify({event: 'userAuth', data: {result: false, message: 'User not found.'}}));
                else if(user.banned)
                    return clients[ID].socket.sendUTF(JSON.stringify({event: 'userAuth', data: {result: false, message: 'You are banned.'}}));
                else if(user.accessLevel < 1)
                    return clients[ID].socket.sendUTF(JSON.stringify({event: 'userAuth', data: {result: false, message: 'Your access level is not enough.'}}));
                var token = jwt.sign({id: user._id},config.secret,{
                    expiresIn : 12 * 60 * 60 * 1000 // 12 hours
                })
                clients[ID].loggedin = true;
                clients[ID].userId = user._id;
                clients[ID].isRobot = false;
                clients[ID].accessLevel = user.accessLevel;

                clients[ID].socket.sendUTF(JSON.stringify({event: 'userAuth', data: {result: true, token: token, message: "Succesfully logged in.", banned: user.banned, accessLevel: user.accessLevel}}));
                clients[ID].socket.sendUTF(JSON.stringify({event: "map", data: MAP}));
                for(var rId in robots) {
                    clients[ID].socket.sendUTF(JSON.stringify({event: "odometry", data: robots[rId].odometry}));
                }
                if(clients[ID].accessLevel >= 3) {
                    Robot.find({},function(err, robotsDB) {
                        if(!err)
                                clients[ID].socket.sendUTF(JSON.stringify({event: "robotsList", data: robotsDB}));
                    });
                }
            });
        }
        else
            clients[ID].socket.sendUTF(JSON.stringify({event: "userAuth", data: {result: false, message: "Username or password is not provided."}}));
    }
    else
        clients[ID].socket.sendUTF(JSON.stringify({event: "userAuth", data: {result: false, message: "You are already registered and logged in." }}));
}

module.exports.handle = handle;