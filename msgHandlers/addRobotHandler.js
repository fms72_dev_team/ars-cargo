var Robot = require('../models/robot.js');
var jwt = require('jsonwebtoken');
var config = require("../config.json");

function handle (data, ID, clients) {
    if(clients[ID].loggedin) {
        if(!clients[ID].isRobot) {
            if(clients[ID].accessLevel >= 3) {
                if(typeof(data.name) == "string" && typeof(data.class) == "number") {
                    var token = jwt.sign({name: data.name, class: data.class, time: Date.now()}, config.secret);
                    var newRobot = new Robot({
                        name: data.name,
                        token: token,
                        class: data.class
                    });
                    newRobot.save(function(err) {
                        if(err) {
                            console.log(err);
                            clients[ID].socket.sendUTF(JSON.stringify({event: "addRobot", data: {result: false, message: "Name of the robot must be unique."}}));
                        }
                        else {
                            clients[ID].socket.sendUTF(JSON.stringify({event: "addRobot", data: {result: true }}));
                            Robot.find({},function(err, robotsDB) {
                                if(!err)
                                    for(var uId in clients)
                                        if(clients[uId].loggedin && !clients[uId].isRobot && clients[uId].accessLevel >= 3)
                                            clients[uId].socket.sendUTF(JSON.stringify({event: "robotsList", data: robotsDB}));
                            });
                        }
                    })
                }
                else
                    clients[ID].socket.sendUTF(JSON.stringify({event: "addRobot", data: {result: false, message: "Your access level is not enough."}}));
            }
            else
                clients[ID].socket.sendUTF(JSON.stringify({event: "addRobot", data: {result: false, message: "Username or password is not provided."}}));
        }
    }
    else
        clients[ID].socket.sendUTF(JSON.stringify({event: "addRobot", data: {result: false, message: "You must be logged in." }}));
}

module.exports.handle = handle;