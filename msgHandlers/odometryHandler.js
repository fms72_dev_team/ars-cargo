function handle (data, ID, clients, robots) {
    if(clients[ID].loggedin) {
        if(clients[ID].isRobot) {
            if(data.odometry) {
                if(data.odometry.pose && data.odometry.pose.position && data.odometry.pose.rotation && data.odometry.velocity && data.odometry.velocity.linear && data.odometry.velocity.angular) {
                    robots[clients[ID].robotId].odometry = data.odometry;
                    data.odometry.robotInfo = {
                        id: clients[ID].robotId,
                        name: robots[clients[ID].robotId].name,
                        class: robots[clients[ID].robotId].class
                    }
                    for(var uId in clients) {
                        if(clients[uId].loggedin && !clients[uId].isRobot)
                            clients[uId].socket.sendUTF(JSON.stringify({event: "odometry", data: data.odometry}));
                    }
                }
                else
                    clients[ID].socket.sendUTF(JSON.stringify({event: "odometry", data: {result: false, message: "Incorrect odometry."}}));
            }
            else
                clients[ID].socket.sendUTF(JSON.stringify({event: "odometry", data: {result: false, message: "No odometry provided."}}));
        }
        else
            clients[ID].socket.sendUTF(JSON.stringify({event: "odometry", data: {result: false, message: "You are not a robot."}}));
    }
    else
        clients[ID].socket.sendUTF(JSON.stringify({event: "odometry", data: {result: false, message: "You are not logged in."}}));
}

module.exports.handle = handle;