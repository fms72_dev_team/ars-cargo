var Robot = require('../models/robot.js');
var jwt = require('jsonwebtoken');
var config = require("../config.json");

function handle (data, ID, clients) {
    if(clients[ID].loggedin) {
        if(!clients[ID].isRobot) {
            if(clients[ID].accessLevel >= 3 ) {
                if(typeof(data.token) == "string") {
                    clients[ID].socket.sendUTF(JSON.stringify({event: "removeRobot", data: {result: true }}));
                    Robot.findOneAndRemove({token: data.token}, function(err) {
                        if(err)
                            return clients[ID].socket.sendUTF(JSON.stringify({event: "removeRobot", data: {result: false, message: "Robot not found."}}));
                        clients[ID].socket.sendUTF(JSON.stringify({event: "removeRobot", data: {result: true }}));
                        Robot.find({},function(err, robotsDB) {
                            if(!err)
                                for(var uId in clients)
                                    if(clients[uId].loggedin && !clients[uId].isRobot && clients[uId].accessLevel >= 3)
                                        clients[uId].socket.sendUTF(JSON.stringify({event: "robotsList", data: robotsDB}));
                        });
                    });
                }
                else
                    clients[ID].socket.sendUTF(JSON.stringify({event: "removeRobot", data: {result: false, message: "Username or password is not provided."}}));
            }
            else
                clients[ID].socket.sendUTF(JSON.stringify({event: "removeRobot", data: {result: false, message: "Your access level is not enough."}}));
        }
    }
    else
        clients[ID].socket.sendUTF(JSON.stringify({event: "removeRobot", data: {result: false, message: "You must be logged in." }}));
}

module.exports.handle = handle;