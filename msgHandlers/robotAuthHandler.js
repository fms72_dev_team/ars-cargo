var Robot = require('../models/robot.js');
var emptyOdometry = require("../emptyOdometry.json");

function handle (data, ID, clients, robots) {
    if(!clients[ID].loggedin) {
        if(data.token) {
            Robot.findOne({token: data.token}, function (err, robot) {
                if(!robot)
                    return clients[ID].socket.sendUTF(JSON.stringify({event: 'robotAuth', data: {result: false, message: 'Robot not found.'}}));
                else if(robot.banned)
                    return clients[ID].socket.sendUTF(JSON.stringify({event: 'robotAuth', data: {result: false, message: 'You are disabled.'}}));
                var token = jwt.sign({id: robot._id},config.secret,{
                    expiresIn : 12 * 60 * 60 * 1000 // 12 hours
                })
                clients[ID].loggedin = true;
                clients[ID].robotId = robot._id;
                clients[ID].isRobot = true;

                robots[robot._id] = {
                    client: clients[ID],
                    class: robot.class,
                    odometry: emptyOdometry,
                    name: robot.name
                }

                clients[ID].socket.sendUTF(JSON.stringify({event: 'robotAuth', data: {result: true, token: token, message: "Succesfully logged in.", name: robot.name, disabled: robot.disabled, class: robot.class}}));
                clients[ID].socket.sendUTF(JSON.stringify({event: "map", data: MAP}));
            });
        }
        else
            clients[ID].socket.sendUTF(JSON.stringify({event: "robotAuth", data: {result: false, message: "Username or password is not provided."}}));
    }
    else
        clients[ID].socket.sendUTF(JSON.stringify({event: "robotAuth", data: {result: false, message: "You are already registered and logged in." }}));
}

module.exports.handle = handle;